// Load all required modules for uploading files
var fs = require("fs"),
    path = require("path"),
    multer = require("multer");
    // storage = multer.diskStorage({
    //     // Storage configuration for multer - where to save and under what name
    //     // destination: './uploads_tmp/',
    //     destination: __dirname + './../client/uploads',
    //     filename: function (req, file, cb) {
    //         cb(null, Date.now() + '-' + file.originalname);
    //     }
    // }),
    // upload = multer({
    //     // Actual upload function using previously defined configuration
    //     storage: storage
    // });

    // Import S3 packages
    var config = require('./config');
    var AWS = require('aws-sdk');
    var multerS3 = require('multer-s3');

    // Configure AWS and multer-S3    
    AWS.config.region = config.AWS_S3_REGION;

    // AWS.config.update({
    //     accessKeyId: accessKeyId,
    //     secretAccessKey: secretAccessKey
    // });
    
    var s3Bucket = new AWS.S3();

    var uploadS3 = multer({
        storage: multerS3({
            s3: s3Bucket,
            bucket: config.AWS_S3_BUCKET,
            metadata: function(req, file, cb) {
                console.log("config")
                console.log(AWS.config)
                cb(null, {fieldName: file.fieldname});
            },
            key: function(req, file, cb) {
                console.log("filename!! " + file.originalname);
                cb(null, Date.now() + '-' + file.originalname);   
            }
        })
    })

// Export the module to be used(AKA required) in app.js
module.exports = function (app) {
    // POST request handler for S3 Upload
    app.post("/uploadS3", 
        uploadS3.single("img-file"),
        function(req, res) {
            console.log("Upload to S3...");
            console.log(req.file);
                // Sends a HTTP 202 status code for Accepted to browser and a json containing the image file size to be displayed in the flash message
                res.status(202).json({
                    path: req.file.location
                });
        }
    );
}
(function(){
    // Angular app named UploadApp with one dependency
    angular
        .module("UploadApp", ["ngFileUpload"]);
})();